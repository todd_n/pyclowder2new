#!/usr/bin/env python

"""Example extractor based on the clowder code."""

import logging
import subprocess
import hyperspy.api as hs


from pyclowder.extractors import Extractor
import pyclowder.files


class HyperSpyDm3(Extractor):
    """Count the number of characters, words and lines in a text file."""
    def __init__(self):
        Extractor.__init__(self)

        # add any additional arguments to parser
        # self.parser.add_argument('--max', '-m', type=int, nargs='?', default=-1,
        #                          help='maximum number (default=-1)')

        # parse command line and load default logging configuration
        self.setup()

        # setup logging for the exctractor
        logging.getLogger('pyclowder').setLevel(logging.DEBUG)
        logging.getLogger('__main__').setLevel(logging.DEBUG)

    def process_message(self, connector, host, secret_key, resource, parameters):
        # Process the file and upload the results

        logger = logging.getLogger(__name__)
        inputfile = resource["local_paths"][0]
        file_id = resource['id']

        # call actual program
        metadata = dict()
        f = hs.load(inputfile)
        dic_image_data = f.original_metadata.ImageList.TagGroup0.ImageData.as_dictionary()
        dic_image_tags = f.original_metadata.ImageList.TagGroup0.ImageTags.as_dictionary()
        metadata['Image_Data'] = dic_image_data
        metadata['Image_Tags'] = dic_image_tags
        result = metadata
        metadata = self.get_metadata(result, 'file', file_id, host)
        logger.debug(metadata)

        # upload metadata
        pyclowder.files.upload_metadata(connector, host, secret_key, file_id, metadata)


if __name__ == "__main__":
    extractor = HyperSpyDm3()
    extractor.start()
